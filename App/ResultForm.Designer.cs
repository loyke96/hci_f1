﻿namespace FormulaOne
{
    partial class ResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grandPrixListBox = new System.Windows.Forms.ListBox();
            this.dataPanel = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.teamDataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.driverDataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.raceDataGridView = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.yearNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.dataPanel.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teamDataGridView)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.driverDataGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.raceDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // grandPrixListBox
            // 
            this.grandPrixListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.grandPrixListBox.DisplayMember = "Name";
            this.grandPrixListBox.ForeColor = System.Drawing.Color.Black;
            this.grandPrixListBox.FormattingEnabled = true;
            this.grandPrixListBox.Location = new System.Drawing.Point(4, 6);
            this.grandPrixListBox.Name = "grandPrixListBox";
            this.grandPrixListBox.Size = new System.Drawing.Size(211, 288);
            this.grandPrixListBox.TabIndex = 0;
            this.grandPrixListBox.ValueMember = "ID";
            this.grandPrixListBox.SelectedIndexChanged += new System.EventHandler(this.GrandPrixListBox_SelectedIndexChanged);
            // 
            // dataPanel
            // 
            this.dataPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataPanel.Controls.Add(this.groupBox2);
            this.dataPanel.Controls.Add(this.groupBox3);
            this.dataPanel.Controls.Add(this.groupBox1);
            this.dataPanel.Location = new System.Drawing.Point(219, 3);
            this.dataPanel.Name = "dataPanel";
            this.dataPanel.Size = new System.Drawing.Size(729, 358);
            this.dataPanel.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.teamDataGridView);
            this.groupBox2.Location = new System.Drawing.Point(418, 193);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(308, 162);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Team Standings";
            // 
            // teamDataGridView
            // 
            this.teamDataGridView.AllowUserToAddRows = false;
            this.teamDataGridView.AllowUserToDeleteRows = false;
            this.teamDataGridView.AllowUserToResizeColumns = false;
            this.teamDataGridView.AllowUserToResizeRows = false;
            this.teamDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.teamDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.teamDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.teamDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            this.teamDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.teamDataGridView.ColumnHeadersVisible = false;
            this.teamDataGridView.Location = new System.Drawing.Point(6, 19);
            this.teamDataGridView.Name = "teamDataGridView";
            this.teamDataGridView.ReadOnly = true;
            this.teamDataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.teamDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.teamDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.teamDataGridView.Size = new System.Drawing.Size(296, 137);
            this.teamDataGridView.TabIndex = 0;
            this.teamDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DataGridView_CellFormatting);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.driverDataGridView);
            this.groupBox3.Location = new System.Drawing.Point(418, 9);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(308, 178);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Driver Standings";
            // 
            // driverDataGridView
            // 
            this.driverDataGridView.AllowUserToAddRows = false;
            this.driverDataGridView.AllowUserToDeleteRows = false;
            this.driverDataGridView.AllowUserToResizeColumns = false;
            this.driverDataGridView.AllowUserToResizeRows = false;
            this.driverDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.driverDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.driverDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.driverDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            this.driverDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.driverDataGridView.ColumnHeadersVisible = false;
            this.driverDataGridView.Location = new System.Drawing.Point(6, 19);
            this.driverDataGridView.Name = "driverDataGridView";
            this.driverDataGridView.ReadOnly = true;
            this.driverDataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.driverDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.driverDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.driverDataGridView.Size = new System.Drawing.Size(296, 153);
            this.driverDataGridView.TabIndex = 0;
            this.driverDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DataGridView_CellFormatting);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.raceDataGridView);
            this.groupBox1.Location = new System.Drawing.Point(3, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(409, 346);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Race Standings";
            // 
            // raceDataGridView
            // 
            this.raceDataGridView.AllowUserToAddRows = false;
            this.raceDataGridView.AllowUserToDeleteRows = false;
            this.raceDataGridView.AllowUserToResizeColumns = false;
            this.raceDataGridView.AllowUserToResizeRows = false;
            this.raceDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.raceDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.raceDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.raceDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            this.raceDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.raceDataGridView.ColumnHeadersVisible = false;
            this.raceDataGridView.Location = new System.Drawing.Point(6, 19);
            this.raceDataGridView.Name = "raceDataGridView";
            this.raceDataGridView.ReadOnly = true;
            this.raceDataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.raceDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.raceDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.raceDataGridView.Size = new System.Drawing.Size(397, 321);
            this.raceDataGridView.TabIndex = 0;
            this.raceDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DataGridView_CellFormatting);
            // 
            // label1
            // 
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Location = new System.Drawing.Point(28, 319);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Season";
            // 
            // yearNumericUpDown
            // 
            this.yearNumericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.yearNumericUpDown.Location = new System.Drawing.Point(106, 320);
            this.yearNumericUpDown.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.yearNumericUpDown.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.yearNumericUpDown.Name = "yearNumericUpDown";
            this.yearNumericUpDown.Size = new System.Drawing.Size(65, 16);
            this.yearNumericUpDown.TabIndex = 3;
            this.yearNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.yearNumericUpDown.Value = new decimal(new int[] {
            2015,
            0,
            0,
            0});
            this.yearNumericUpDown.ValueChanged += new System.EventHandler(this.YearNumericUpDown_ValueChanged);
            // 
            // ResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(952, 364);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.yearNumericUpDown);
            this.Controls.Add(this.grandPrixListBox);
            this.Controls.Add(this.dataPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ResultForm";
            this.Text = "DriverForm";
            this.Load += new System.EventHandler(this.ResultForm_Load);
            this.dataPanel.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teamDataGridView)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.driverDataGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.raceDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearNumericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListBox grandPrixListBox;
        private System.Windows.Forms.Panel dataPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown yearNumericUpDown;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView teamDataGridView;
        private System.Windows.Forms.DataGridView driverDataGridView;
        private System.Windows.Forms.DataGridView raceDataGridView;
    }
}