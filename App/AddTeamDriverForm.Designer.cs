﻿namespace FormulaOne {
    partial class AddTeamDriverForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddTeamDriverForm));
            this.teamComboBox = new System.Windows.Forms.ComboBox();
            this.driverComboBox = new System.Windows.Forms.ComboBox();
            this.driverPictureBox = new System.Windows.Forms.PictureBox();
            this.teamPictureBox = new System.Windows.Forms.PictureBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.newDriverButton = new System.Windows.Forms.Button();
            this.newTeamButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.driverPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // teamComboBox
            // 
            this.teamComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.teamComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.teamComboBox.FormattingEnabled = true;
            this.teamComboBox.Location = new System.Drawing.Point(296, 319);
            this.teamComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.teamComboBox.Name = "teamComboBox";
            this.teamComboBox.Size = new System.Drawing.Size(259, 25);
            this.teamComboBox.TabIndex = 1;
            this.teamComboBox.SelectedIndexChanged += new System.EventHandler(this.TeamComboBox_SelectedIndexChanged);
            // 
            // driverComboBox
            // 
            this.driverComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.driverComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.driverComboBox.FormattingEnabled = true;
            this.driverComboBox.Location = new System.Drawing.Point(12, 319);
            this.driverComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.driverComboBox.Name = "driverComboBox";
            this.driverComboBox.Size = new System.Drawing.Size(260, 25);
            this.driverComboBox.TabIndex = 0;
            this.driverComboBox.SelectedValueChanged += new System.EventHandler(this.DriverComboBox_SelectedValueChanged);
            // 
            // driverPictureBox
            // 
            this.driverPictureBox.Location = new System.Drawing.Point(12, 12);
            this.driverPictureBox.Name = "driverPictureBox";
            this.driverPictureBox.Size = new System.Drawing.Size(260, 300);
            this.driverPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.driverPictureBox.TabIndex = 5;
            this.driverPictureBox.TabStop = false;
            // 
            // teamPictureBox
            // 
            this.teamPictureBox.Location = new System.Drawing.Point(296, 12);
            this.teamPictureBox.Name = "teamPictureBox";
            this.teamPictureBox.Size = new System.Drawing.Size(260, 300);
            this.teamPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.teamPictureBox.TabIndex = 6;
            this.teamPictureBox.TabStop = false;
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.Red;
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.ForeColor = System.Drawing.Color.White;
            this.cancelButton.Location = new System.Drawing.Point(436, 351);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(120, 30);
            this.cancelButton.TabIndex = 5;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // okButton
            // 
            this.okButton.BackColor = System.Drawing.Color.Black;
            this.okButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.okButton.ForeColor = System.Drawing.Color.White;
            this.okButton.Location = new System.Drawing.Point(296, 351);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(120, 30);
            this.okButton.TabIndex = 4;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = false;
            this.okButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // newDriverButton
            // 
            this.newDriverButton.BackColor = System.Drawing.Color.Black;
            this.newDriverButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newDriverButton.ForeColor = System.Drawing.Color.White;
            this.newDriverButton.Location = new System.Drawing.Point(12, 351);
            this.newDriverButton.Name = "newDriverButton";
            this.newDriverButton.Size = new System.Drawing.Size(120, 30);
            this.newDriverButton.TabIndex = 2;
            this.newDriverButton.Text = "NEW DRIVER";
            this.newDriverButton.UseVisualStyleBackColor = false;
            this.newDriverButton.Click += new System.EventHandler(this.NewDriverButton_Click);
            // 
            // newTeamButton
            // 
            this.newTeamButton.BackColor = System.Drawing.Color.Black;
            this.newTeamButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newTeamButton.ForeColor = System.Drawing.Color.White;
            this.newTeamButton.Location = new System.Drawing.Point(152, 351);
            this.newTeamButton.Name = "newTeamButton";
            this.newTeamButton.Size = new System.Drawing.Size(120, 30);
            this.newTeamButton.TabIndex = 3;
            this.newTeamButton.Text = "NEW TEAM";
            this.newTeamButton.UseVisualStyleBackColor = false;
            this.newTeamButton.Click += new System.EventHandler(this.NewTeamButton_Click);
            // 
            // AddTeamDriverForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(568, 392);
            this.Controls.Add(this.newTeamButton);
            this.Controls.Add(this.newDriverButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.teamPictureBox);
            this.Controls.Add(this.driverPictureBox);
            this.Controls.Add(this.driverComboBox);
            this.Controls.Add(this.teamComboBox);
            this.Font = new System.Drawing.Font("Yu Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "AddTeamDriverForm";
            this.Text = "Add team driver";
            this.Load += new System.EventHandler(this.AddExistingDriverForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.driverPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ComboBox teamComboBox;
        private System.Windows.Forms.ComboBox driverComboBox;
        private System.Windows.Forms.PictureBox driverPictureBox;
        private System.Windows.Forms.PictureBox teamPictureBox;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button newDriverButton;
        private System.Windows.Forms.Button newTeamButton;
    }
}