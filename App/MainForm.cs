﻿using FormulaOne.Properties;
using System;
using System.Windows.Forms;

namespace FormulaOne
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void DriverButton_Click(object sender, EventArgs e)
        {
            var driverForm = new DriverForm
            {
                TopLevel = false
            };
            switchPanel.Controls.Clear();
            switchPanel.Controls.Add(driverForm);
            driverForm.Show();
        }

        private void TeamButton_Click(object sender, EventArgs e)
        {
            var teamForm = new TeamForm
            {
                TopLevel = false
            };
            switchPanel.Controls.Clear();
            switchPanel.Controls.Add(teamForm);
            teamForm.Show();
        }

        private void CircuitButton_Click(object sender, EventArgs e)
        {
            var circuitForm = new CircuitForm
            {
                TopLevel = false
            };
            switchPanel.Controls.Clear();
            switchPanel.Controls.Add(circuitForm);
            circuitForm.Show();
        }

        private void RaceButton_Click(object sender, EventArgs e)
        {
            var raceForm = new RaceForm
            {
                TopLevel = false
            };
            switchPanel.Controls.Clear();
            switchPanel.Controls.Add(raceForm);
            raceForm.Show();
        }

        private void ResultButton_Click(object sender, EventArgs e)
        {
            var resultForm = new ResultForm
            {
                TopLevel = false
            };
            switchPanel.Controls.Clear();
            switchPanel.Controls.Add(resultForm);
            resultForm.Show();
        }

        private void SwitchPanel_ControlAdded(object sender, ControlEventArgs e)
        {
            BackgroundImage = Resources.simple;
            switchPanel.ControlAdded -= SwitchPanel_ControlAdded;
        }
    }
}
