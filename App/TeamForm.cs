﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace FormulaOne
{
    public partial class TeamForm : Form
    {
        private object firstDriver;
        private object secondDriver;

        public TeamForm()
        {
            InitializeComponent();
            yearNumericUpDown.Value
                 = DateTime.Today.Year;
        }

        public TeamForm(int year, object team)
        {
            InitializeComponent();
            yearNumericUpDown.Value = year;
            teamListBox.SelectedItem = team;
        }

        private void TeamForm_Load(object sender, EventArgs e)
        {
            yearNumericUpDown.Maximum
                = DateTime.Today.Year;
        }

        private void TeamYearNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
            connection.Open();
            var previusSelected = teamListBox.SelectedItem;
            var command = connection.CreateCommand();
            command.CommandText = "SELECT DISTINCT(ID), ALIAS FROM TEAM JOIN `TEAM DRIVER` ON TEAM.ID = `TEAM DRIVER`.TEAM_ID WHERE SEASON = @SEASON";
            command.Parameters.AddWithValue("@SEASON", yearNumericUpDown.Value);
            var reader = command.ExecuteReader();
            var teams = new List<object>();
            while (reader.Read())
            {
                teams.Add(new
                {
                    ID = reader.GetInt32(0),
                    Name = reader.GetString(1)
                });
            }
            reader.Close();
            teamListBox.DataSource = teams;
            teamListBox.ValueMember = "ID";
            teamListBox.DisplayMember = "Name";
            if (teams.Contains(previusSelected)) teamListBox.SelectedItem = previusSelected;
            if (teams.Count == 0) dataPanel.Hide();
            connection.Close();
        }

        private void TeamListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (teamListBox.SelectedItem == null)
            {
                dataPanel.Hide();
            }
            else
            {
                var buffer = new byte[1_024 * 1_024];
                var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "SELECT ALIAS, `FULL NAME`, BASE, EMBLEM, C.NAME, FLAG FROM TEAM T JOIN COUNTRY C ON T.COUNTRY_ID = C.ID WHERE T.ID = @ID";
                command.Parameters.AddWithValue("@ID", teamListBox.SelectedValue);
                var reader = command.ExecuteReader();
                reader.Read();
                teamAliasLabel.Text = reader.GetString(0);
                teamBaseLabel.Text = $"Base:         {reader.GetString(2)}, {reader.GetString(4)}";
                teamNameLabel.Text = reader.GetString(1);
                reader.GetBytes(3, 0, buffer, 0, buffer.Length);
                teamEmblemPictureBox.Image = Image.FromStream(new MemoryStream(buffer));
                reader.GetBytes(5, 0, buffer, 0, buffer.Length);
                countyFlagPictureBox.Image = Image.FromStream(new MemoryStream(buffer));
                reader.Close();

                command.CommandText = "SELECT `RANK`, POINTS FROM TEAM_POINTS WHERE SEASON = @SEASON AND TEAM_ID = @ID";
                command.Parameters.AddWithValue("@SEASON", yearNumericUpDown.Value);
                reader = command.ExecuteReader();
                reader.Read();
                teamRankLabel.Text = $"Rank: {reader.GetString(0)}";
                teamPointsLabel.Text = $"Points: {reader.GetString(1)}";
                reader.Close();

                command.CommandText = "SELECT ID, `NAME`, `RANK`, PICTURE FROM TEAM_DRIVERS WHERE TEAM_ID = @ID AND SEASON = @SEASON";
                reader = command.ExecuteReader();
                if (reader.Read())
                {
                    reader.GetBytes(3, 0, buffer, 0, buffer.Length);
                    firstTeamDriverPictureBox.Image = Image.FromStream(new MemoryStream(buffer));
                    firstTeamDriverNameLabel.Text = reader.GetString(1);
                    firstTeamDriverRankLabel.Text = $"Rank: {reader.GetString(2)}";
                    firstDriver = new
                    {
                        ID = reader.GetInt32(0),
                        Name = firstTeamDriverNameLabel.Text
                    };
                }

                if (reader.Read())
                {
                    reader.GetBytes(3, 0, buffer, 0, buffer.Length);
                    secondTeamDriverPictureBox.Image = Image.FromStream(new MemoryStream(buffer));
                    secondTeamDriverNameLabel.Text = reader.GetString(1);
                    secondTeamDriverRankLabel.Text = $"Rank: {reader.GetString(2)}";
                    secondDriver = new
                    {
                        ID = reader.GetInt32(0),
                        Name = secondTeamDriverNameLabel.Text
                    };
                }
                else
                {
                    secondTeamDriverPictureBox.Image = null;
                    secondTeamDriverNameLabel.Text = "No driver";
                    secondTeamDriverRankLabel.Text = "";
                    secondDriver = null;
                }
                reader.Close();

                dataPanel.Show();
                connection.Close();
            }
        }

        private void ChangeEmblemButton_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Image Files|*.bmp;*.png;*.jpg;*.jpeg"
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                var memoryStream = new MemoryStream();
                var photo = Image.FromFile(openFileDialog.FileName);
                photo.Save(memoryStream, ImageFormat.Jpeg);
                var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "UPDATE TEAM SET EMBLEM = @EMBLEM WHERE ID = @ID";
                command.Parameters.AddWithValue("@EMBLEM", memoryStream.ToArray());
                command.Parameters.AddWithValue("@ID", teamListBox.SelectedValue);
                command.ExecuteNonQuery();
                connection.Close();
                teamEmblemPictureBox.Image = photo;
            }
        }

        private void AddTeamDriverButton_Click(object sender, EventArgs e)
        {
            if (new AddTeamDriverForm
            {
                Season = (int)yearNumericUpDown.Value
            }.ShowDialog() == DialogResult.OK)
            {
                TeamYearNumericUpDown_ValueChanged(null, null);
            }
        }

        private void FirstTeamDriverPictureBox_Click(object sender, EventArgs e)
        {
            Controls.Clear();
            var driverForm = new DriverForm((int)yearNumericUpDown.Value, firstDriver)
            {
                TopLevel = false
            };
            Controls.Add(driverForm);
            driverForm.Show();
        }

        private void SecondTeamDriverPictureBox_Click(object sender, EventArgs e)
        {
            if (secondDriver != null)
            {
                Controls.Clear();
                var driverForm = new DriverForm((int)yearNumericUpDown.Value, secondDriver)
                {
                    TopLevel = false
                };
                Controls.Add(driverForm);
                driverForm.Show();
            }
        }
    }
}
