﻿namespace FormulaOne
{
    partial class DriverForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addTeamDriverButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.yearNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.driverListBox = new System.Windows.Forms.ListBox();
            this.dataPanel = new System.Windows.Forms.Panel();
            this.changePhotoButton = new System.Windows.Forms.Button();
            this.flagPictureBox = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.seasonPodiumsLabel = new System.Windows.Forms.Label();
            this.seasonBestFinishLabel = new System.Windows.Forms.Label();
            this.emblemPictureBox = new System.Windows.Forms.PictureBox();
            this.teamLabel = new System.Windows.Forms.Label();
            this.rankingLabel = new System.Windows.Forms.Label();
            this.pointsLabel = new System.Windows.Forms.Label();
            this.driverPictureBox = new System.Windows.Forms.PictureBox();
            this.driverCountryLabel = new System.Windows.Forms.Label();
            this.driverNameLabel = new System.Windows.Forms.Label();
            this.driverBornLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lastRaceLabel = new System.Windows.Forms.Label();
            this.lastRaceRankLabel = new System.Windows.Forms.Label();
            this.grandPrixEnteredLabel = new System.Windows.Forms.Label();
            this.careerBestFinishLabel = new System.Windows.Forms.Label();
            this.careerPodiumsLabel = new System.Windows.Forms.Label();
            this.careerBestRankLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.yearNumericUpDown)).BeginInit();
            this.dataPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flagPictureBox)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.emblemPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.driverPictureBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // addTeamDriverButton
            // 
            this.addTeamDriverButton.AutoSize = true;
            this.addTeamDriverButton.BackColor = System.Drawing.Color.Black;
            this.addTeamDriverButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addTeamDriverButton.ForeColor = System.Drawing.Color.White;
            this.addTeamDriverButton.Location = new System.Drawing.Point(39, 325);
            this.addTeamDriverButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.addTeamDriverButton.Name = "addTeamDriverButton";
            this.addTeamDriverButton.Size = new System.Drawing.Size(140, 30);
            this.addTeamDriverButton.TabIndex = 4;
            this.addTeamDriverButton.Text = "ADD TEAM DRIVER";
            this.addTeamDriverButton.UseVisualStyleBackColor = false;
            this.addTeamDriverButton.Click += new System.EventHandler(this.AddTeamDriverButton_Click);
            // 
            // label1
            // 
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Location = new System.Drawing.Point(36, 301);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Season";
            // 
            // yearNumericUpDown
            // 
            this.yearNumericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.yearNumericUpDown.Location = new System.Drawing.Point(114, 302);
            this.yearNumericUpDown.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.yearNumericUpDown.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.yearNumericUpDown.Name = "yearNumericUpDown";
            this.yearNumericUpDown.Size = new System.Drawing.Size(65, 16);
            this.yearNumericUpDown.TabIndex = 3;
            this.yearNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.yearNumericUpDown.Value = new decimal(new int[] {
            2015,
            0,
            0,
            0});
            this.yearNumericUpDown.ValueChanged += new System.EventHandler(this.DriverYearNumericUpDown_ValueChanged);
            // 
            // driverListBox
            // 
            this.driverListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.driverListBox.DisplayMember = "Name";
            this.driverListBox.ForeColor = System.Drawing.Color.Black;
            this.driverListBox.FormattingEnabled = true;
            this.driverListBox.Location = new System.Drawing.Point(4, 6);
            this.driverListBox.Name = "driverListBox";
            this.driverListBox.Size = new System.Drawing.Size(211, 288);
            this.driverListBox.TabIndex = 0;
            this.driverListBox.ValueMember = "ID";
            this.driverListBox.SelectedIndexChanged += new System.EventHandler(this.DriverListBox_SelectedIndexChanged);
            // 
            // dataPanel
            // 
            this.dataPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataPanel.Controls.Add(this.changePhotoButton);
            this.dataPanel.Controls.Add(this.flagPictureBox);
            this.dataPanel.Controls.Add(this.groupBox2);
            this.dataPanel.Controls.Add(this.driverPictureBox);
            this.dataPanel.Controls.Add(this.driverCountryLabel);
            this.dataPanel.Controls.Add(this.driverNameLabel);
            this.dataPanel.Controls.Add(this.driverBornLabel);
            this.dataPanel.Controls.Add(this.groupBox1);
            this.dataPanel.Location = new System.Drawing.Point(219, 3);
            this.dataPanel.Name = "dataPanel";
            this.dataPanel.Size = new System.Drawing.Size(729, 358);
            this.dataPanel.TabIndex = 1;
            // 
            // changePhotoButton
            // 
            this.changePhotoButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.changePhotoButton.AutoSize = true;
            this.changePhotoButton.BackColor = System.Drawing.Color.Black;
            this.changePhotoButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.changePhotoButton.ForeColor = System.Drawing.Color.White;
            this.changePhotoButton.Location = new System.Drawing.Point(150, 322);
            this.changePhotoButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.changePhotoButton.Name = "changePhotoButton";
            this.changePhotoButton.Size = new System.Drawing.Size(138, 30);
            this.changePhotoButton.TabIndex = 3;
            this.changePhotoButton.Text = "CHANGE PHOTO";
            this.changePhotoButton.UseVisualStyleBackColor = false;
            this.changePhotoButton.Click += new System.EventHandler(this.ChangePhotoButton_Click);
            // 
            // flagPictureBox
            // 
            this.flagPictureBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.flagPictureBox.Location = new System.Drawing.Point(62, 5);
            this.flagPictureBox.Name = "flagPictureBox";
            this.flagPictureBox.Size = new System.Drawing.Size(25, 25);
            this.flagPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.flagPictureBox.TabIndex = 8;
            this.flagPictureBox.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox2.Controls.Add(this.seasonPodiumsLabel);
            this.groupBox2.Controls.Add(this.seasonBestFinishLabel);
            this.groupBox2.Controls.Add(this.emblemPictureBox);
            this.groupBox2.Controls.Add(this.teamLabel);
            this.groupBox2.Controls.Add(this.rankingLabel);
            this.groupBox2.Controls.Add(this.pointsLabel);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox2.Location = new System.Drawing.Point(298, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(172, 352);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Season";
            // 
            // seasonPodiumsLabel
            // 
            this.seasonPodiumsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.seasonPodiumsLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.seasonPodiumsLabel.Location = new System.Drawing.Point(3, 283);
            this.seasonPodiumsLabel.Name = "seasonPodiumsLabel";
            this.seasonPodiumsLabel.Size = new System.Drawing.Size(163, 18);
            this.seasonPodiumsLabel.TabIndex = 3;
            this.seasonPodiumsLabel.Text = "Podiums";
            this.seasonPodiumsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // seasonBestFinishLabel
            // 
            this.seasonBestFinishLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.seasonBestFinishLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.seasonBestFinishLabel.Location = new System.Drawing.Point(3, 313);
            this.seasonBestFinishLabel.Name = "seasonBestFinishLabel";
            this.seasonBestFinishLabel.Size = new System.Drawing.Size(163, 18);
            this.seasonBestFinishLabel.TabIndex = 4;
            this.seasonBestFinishLabel.Text = "Best finish";
            this.seasonBestFinishLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // emblemPictureBox
            // 
            this.emblemPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.emblemPictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.emblemPictureBox.Location = new System.Drawing.Point(6, 19);
            this.emblemPictureBox.Name = "emblemPictureBox";
            this.emblemPictureBox.Size = new System.Drawing.Size(160, 160);
            this.emblemPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.emblemPictureBox.TabIndex = 10;
            this.emblemPictureBox.TabStop = false;
            this.emblemPictureBox.Click += new System.EventHandler(this.EmblemPictureBox_Click);
            // 
            // teamLabel
            // 
            this.teamLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.teamLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.teamLabel.Location = new System.Drawing.Point(3, 183);
            this.teamLabel.Name = "teamLabel";
            this.teamLabel.Size = new System.Drawing.Size(163, 18);
            this.teamLabel.TabIndex = 0;
            this.teamLabel.Text = "Team";
            this.teamLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rankingLabel
            // 
            this.rankingLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rankingLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rankingLabel.Location = new System.Drawing.Point(3, 223);
            this.rankingLabel.Name = "rankingLabel";
            this.rankingLabel.Size = new System.Drawing.Size(163, 18);
            this.rankingLabel.TabIndex = 1;
            this.rankingLabel.Text = "Rank";
            this.rankingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pointsLabel
            // 
            this.pointsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pointsLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pointsLabel.Location = new System.Drawing.Point(3, 253);
            this.pointsLabel.Name = "pointsLabel";
            this.pointsLabel.Size = new System.Drawing.Size(163, 18);
            this.pointsLabel.TabIndex = 2;
            this.pointsLabel.Text = "Points";
            this.pointsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // driverPictureBox
            // 
            this.driverPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.driverPictureBox.Location = new System.Drawing.Point(476, 3);
            this.driverPictureBox.Name = "driverPictureBox";
            this.driverPictureBox.Size = new System.Drawing.Size(250, 320);
            this.driverPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.driverPictureBox.TabIndex = 2;
            this.driverPictureBox.TabStop = false;
            // 
            // driverCountryLabel
            // 
            this.driverCountryLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.driverCountryLabel.AutoSize = true;
            this.driverCountryLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.driverCountryLabel.Location = new System.Drawing.Point(6, 11);
            this.driverCountryLabel.Name = "driverCountryLabel";
            this.driverCountryLabel.Size = new System.Drawing.Size(56, 13);
            this.driverCountryLabel.TabIndex = 0;
            this.driverCountryLabel.Text = "Nationality";
            this.driverCountryLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // driverNameLabel
            // 
            this.driverNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.driverNameLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.driverNameLabel.Location = new System.Drawing.Point(473, 337);
            this.driverNameLabel.Name = "driverNameLabel";
            this.driverNameLabel.Size = new System.Drawing.Size(250, 18);
            this.driverNameLabel.TabIndex = 5;
            this.driverNameLabel.Text = "Name";
            this.driverNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // driverBornLabel
            // 
            this.driverBornLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.driverBornLabel.AutoSize = true;
            this.driverBornLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.driverBornLabel.Location = new System.Drawing.Point(6, 46);
            this.driverBornLabel.Name = "driverBornLabel";
            this.driverBornLabel.Size = new System.Drawing.Size(29, 13);
            this.driverBornLabel.TabIndex = 1;
            this.driverBornLabel.Text = "Born";
            this.driverBornLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox1.Controls.Add(this.lastRaceLabel);
            this.groupBox1.Controls.Add(this.lastRaceRankLabel);
            this.groupBox1.Controls.Add(this.grandPrixEnteredLabel);
            this.groupBox1.Controls.Add(this.careerBestFinishLabel);
            this.groupBox1.Controls.Add(this.careerPodiumsLabel);
            this.groupBox1.Controls.Add(this.careerBestRankLabel);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(6, 79);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(286, 234);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Career";
            // 
            // lastRaceLabel
            // 
            this.lastRaceLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lastRaceLabel.AutoSize = true;
            this.lastRaceLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lastRaceLabel.Location = new System.Drawing.Point(28, 124);
            this.lastRaceLabel.Name = "lastRaceLabel";
            this.lastRaceLabel.Size = new System.Drawing.Size(51, 13);
            this.lastRaceLabel.TabIndex = 3;
            this.lastRaceLabel.Text = "Last race";
            // 
            // lastRaceRankLabel
            // 
            this.lastRaceRankLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lastRaceRankLabel.AutoSize = true;
            this.lastRaceRankLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lastRaceRankLabel.Location = new System.Drawing.Point(28, 155);
            this.lastRaceRankLabel.Name = "lastRaceRankLabel";
            this.lastRaceRankLabel.Size = new System.Drawing.Size(75, 13);
            this.lastRaceRankLabel.TabIndex = 4;
            this.lastRaceRankLabel.Text = "Last race rank";
            // 
            // grandPrixEnteredLabel
            // 
            this.grandPrixEnteredLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.grandPrixEnteredLabel.AutoSize = true;
            this.grandPrixEnteredLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grandPrixEnteredLabel.Location = new System.Drawing.Point(28, 186);
            this.grandPrixEnteredLabel.Name = "grandPrixEnteredLabel";
            this.grandPrixEnteredLabel.Size = new System.Drawing.Size(95, 13);
            this.grandPrixEnteredLabel.TabIndex = 5;
            this.grandPrixEnteredLabel.Text = "Grand Prix entered";
            // 
            // careerBestFinishLabel
            // 
            this.careerBestFinishLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.careerBestFinishLabel.AutoSize = true;
            this.careerBestFinishLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.careerBestFinishLabel.Location = new System.Drawing.Point(28, 93);
            this.careerBestFinishLabel.Name = "careerBestFinishLabel";
            this.careerBestFinishLabel.Size = new System.Drawing.Size(55, 13);
            this.careerBestFinishLabel.TabIndex = 2;
            this.careerBestFinishLabel.Text = "Best finish";
            // 
            // careerPodiumsLabel
            // 
            this.careerPodiumsLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.careerPodiumsLabel.AutoSize = true;
            this.careerPodiumsLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.careerPodiumsLabel.Location = new System.Drawing.Point(28, 62);
            this.careerPodiumsLabel.Name = "careerPodiumsLabel";
            this.careerPodiumsLabel.Size = new System.Drawing.Size(47, 13);
            this.careerPodiumsLabel.TabIndex = 1;
            this.careerPodiumsLabel.Text = "Podiums";
            // 
            // careerBestRankLabel
            // 
            this.careerBestRankLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.careerBestRankLabel.AutoSize = true;
            this.careerBestRankLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.careerBestRankLabel.Location = new System.Drawing.Point(28, 31);
            this.careerBestRankLabel.Name = "careerBestRankLabel";
            this.careerBestRankLabel.Size = new System.Drawing.Size(52, 13);
            this.careerBestRankLabel.TabIndex = 0;
            this.careerBestRankLabel.Text = "Best rank";
            // 
            // DriverForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(952, 364);
            this.Controls.Add(this.addTeamDriverButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.yearNumericUpDown);
            this.Controls.Add(this.driverListBox);
            this.Controls.Add(this.dataPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DriverForm";
            this.Text = "DriverForm";
            this.Load += new System.EventHandler(this.DriverForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.yearNumericUpDown)).EndInit();
            this.dataPanel.ResumeLayout(false);
            this.dataPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flagPictureBox)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.emblemPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.driverPictureBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addTeamDriverButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown yearNumericUpDown;
        private System.Windows.Forms.ListBox driverListBox;
        private System.Windows.Forms.Panel dataPanel;
        private System.Windows.Forms.Button changePhotoButton;
        private System.Windows.Forms.PictureBox flagPictureBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label seasonPodiumsLabel;
        private System.Windows.Forms.Label seasonBestFinishLabel;
        private System.Windows.Forms.PictureBox emblemPictureBox;
        private System.Windows.Forms.Label teamLabel;
        private System.Windows.Forms.Label rankingLabel;
        private System.Windows.Forms.Label pointsLabel;
        private System.Windows.Forms.PictureBox driverPictureBox;
        private System.Windows.Forms.Label driverCountryLabel;
        private System.Windows.Forms.Label driverNameLabel;
        private System.Windows.Forms.Label driverBornLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lastRaceLabel;
        private System.Windows.Forms.Label lastRaceRankLabel;
        private System.Windows.Forms.Label grandPrixEnteredLabel;
        private System.Windows.Forms.Label careerBestFinishLabel;
        private System.Windows.Forms.Label careerPodiumsLabel;
        private System.Windows.Forms.Label careerBestRankLabel;
    }
}