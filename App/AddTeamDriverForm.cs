﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace FormulaOne
{
    public partial class AddTeamDriverForm : Form
    {
        public int Season { get; set; }
        public AddTeamDriverForm()
        {
            InitializeComponent();
        }

        private void AddExistingDriverForm_Load(object sender, EventArgs e)
        {
            teamComboBox.ValueMember = driverComboBox.ValueMember = "ID";
            driverComboBox.DisplayMember = teamComboBox.DisplayMember = "Name";

            var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
            connection.Open();
            var command = connection.CreateCommand();

            List<object> drivers = new List<object>();
            command.CommandText = "SELECT ID, CONCAT(`FIRST NAME`, ' ', `LAST NAME`) FROM DRIVER WHERE ID NOT IN " +
                "(SELECT DRIVER_ID FROM `TEAM DRIVER` WHERE SEASON = @SEASON)";
            command.Parameters.AddWithValue("@SEASON", Season);
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                drivers.Add(new
                {
                    ID = reader.GetInt32(0),
                    Name = reader.GetString(1)
                });
            }
            reader.Close();

            List<object> teams = new List<object>();
            command.CommandText = "SELECT ID, ALIAS FROM TEAM WHERE " +
              "(SELECT COUNT(*) FROM `TEAM DRIVER` WHERE SEASON = @SEASON AND TEAM_ID = ID) < 2";
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                teams.Add(new
                {
                    ID = reader.GetInt32(0),
                    Name = reader.GetString(1)
                });
            }
            reader.Close();
            connection.Close();
            teamComboBox.DataSource = teams;
            driverComboBox.DataSource = drivers;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void DriverComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (driverComboBox.SelectedValue != null)
            {
                var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "SELECT PICTURE FROM DRIVER WHERE ID = @ID";
                command.Parameters.AddWithValue("@ID", driverComboBox.SelectedValue);
                driverPictureBox.Image = Image.FromStream(new MemoryStream((byte[])command.ExecuteScalar()));
                connection.Close();
            }
        }

        private void TeamComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (teamComboBox.SelectedValue != null)
            {
                var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "SELECT EMBLEM FROM TEAM WHERE ID = @ID";
                command.Parameters.AddWithValue("@ID", teamComboBox.SelectedValue);
                teamPictureBox.Image = Image.FromStream(new MemoryStream((byte[])command.ExecuteScalar()));
                connection.Close();
            }
        }

        private void NewDriverButton_Click(object sender, EventArgs e)
        {
            if (new AddDriverForm().ShowDialog() == DialogResult.OK)
            {
                AddExistingDriverForm_Load(null, null);
            }
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (teamComboBox.SelectedValue != null && driverComboBox.SelectedValue != null)
            {
                var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "INSERT INTO `TEAM DRIVER` VALUES(@DRIVER, @SEASON, @TEAM, 0)";
                command.Parameters.AddWithValue("@TEAM", teamComboBox.SelectedValue);
                command.Parameters.AddWithValue("@DRIVER", driverComboBox.SelectedValue);
                command.Parameters.AddWithValue("@SEASON", Season);
                command.ExecuteNonQuery();
                connection.Close();
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void NewTeamButton_Click(object sender, EventArgs e)
        {
            if (new AddTeamForm().ShowDialog() == DialogResult.OK)
            {
                AddExistingDriverForm_Load(null, null);
            }
        }
    }
}
