﻿namespace FormulaOne
{
    partial class TeamForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.yearNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.teamListBox = new System.Windows.Forms.ListBox();
            this.dataPanel = new System.Windows.Forms.Panel();
            this.secondTeamDriverRankLabel = new System.Windows.Forms.Label();
            this.secondTeamDriverNameLabel = new System.Windows.Forms.Label();
            this.firstTeamDriverRankLabel = new System.Windows.Forms.Label();
            this.firstTeamDriverNameLabel = new System.Windows.Forms.Label();
            this.secondTeamDriverPictureBox = new System.Windows.Forms.PictureBox();
            this.firstTeamDriverPictureBox = new System.Windows.Forms.PictureBox();
            this.changeEmblemButton = new System.Windows.Forms.Button();
            this.countyFlagPictureBox = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.teamEmblemPictureBox = new System.Windows.Forms.PictureBox();
            this.teamAliasLabel = new System.Windows.Forms.Label();
            this.teamRankLabel = new System.Windows.Forms.Label();
            this.teamPointsLabel = new System.Windows.Forms.Label();
            this.teamBaseLabel = new System.Windows.Forms.Label();
            this.teamNameLabel = new System.Windows.Forms.Label();
            this.addTeamDriverButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.yearNumericUpDown)).BeginInit();
            this.dataPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondTeamDriverPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstTeamDriverPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countyFlagPictureBox)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teamEmblemPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Location = new System.Drawing.Point(36, 303);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Season";
            // 
            // yearNumericUpDown
            // 
            this.yearNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.yearNumericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.yearNumericUpDown.Location = new System.Drawing.Point(114, 304);
            this.yearNumericUpDown.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.yearNumericUpDown.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.yearNumericUpDown.Name = "yearNumericUpDown";
            this.yearNumericUpDown.Size = new System.Drawing.Size(65, 16);
            this.yearNumericUpDown.TabIndex = 3;
            this.yearNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.yearNumericUpDown.Value = new decimal(new int[] {
            2015,
            0,
            0,
            0});
            this.yearNumericUpDown.ValueChanged += new System.EventHandler(this.TeamYearNumericUpDown_ValueChanged);
            // 
            // teamListBox
            // 
            this.teamListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.teamListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.teamListBox.DisplayMember = "Name";
            this.teamListBox.ForeColor = System.Drawing.Color.Black;
            this.teamListBox.FormattingEnabled = true;
            this.teamListBox.Location = new System.Drawing.Point(4, 6);
            this.teamListBox.Name = "teamListBox";
            this.teamListBox.Size = new System.Drawing.Size(211, 288);
            this.teamListBox.TabIndex = 0;
            this.teamListBox.ValueMember = "ID";
            this.teamListBox.SelectedIndexChanged += new System.EventHandler(this.TeamListBox_SelectedIndexChanged);
            // 
            // dataPanel
            // 
            this.dataPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataPanel.Controls.Add(this.secondTeamDriverRankLabel);
            this.dataPanel.Controls.Add(this.secondTeamDriverNameLabel);
            this.dataPanel.Controls.Add(this.firstTeamDriverRankLabel);
            this.dataPanel.Controls.Add(this.firstTeamDriverNameLabel);
            this.dataPanel.Controls.Add(this.secondTeamDriverPictureBox);
            this.dataPanel.Controls.Add(this.firstTeamDriverPictureBox);
            this.dataPanel.Controls.Add(this.changeEmblemButton);
            this.dataPanel.Controls.Add(this.countyFlagPictureBox);
            this.dataPanel.Controls.Add(this.groupBox3);
            this.dataPanel.Controls.Add(this.teamBaseLabel);
            this.dataPanel.Controls.Add(this.teamNameLabel);
            this.dataPanel.Location = new System.Drawing.Point(219, 3);
            this.dataPanel.Name = "dataPanel";
            this.dataPanel.Size = new System.Drawing.Size(729, 358);
            this.dataPanel.TabIndex = 1;
            // 
            // secondTeamDriverRankLabel
            // 
            this.secondTeamDriverRankLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.secondTeamDriverRankLabel.Location = new System.Drawing.Point(249, 252);
            this.secondTeamDriverRankLabel.Name = "secondTeamDriverRankLabel";
            this.secondTeamDriverRankLabel.Size = new System.Drawing.Size(203, 18);
            this.secondTeamDriverRankLabel.TabIndex = 4;
            this.secondTeamDriverRankLabel.Text = "label6";
            this.secondTeamDriverRankLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // secondTeamDriverNameLabel
            // 
            this.secondTeamDriverNameLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.secondTeamDriverNameLabel.Location = new System.Drawing.Point(249, 234);
            this.secondTeamDriverNameLabel.Name = "secondTeamDriverNameLabel";
            this.secondTeamDriverNameLabel.Size = new System.Drawing.Size(203, 18);
            this.secondTeamDriverNameLabel.TabIndex = 3;
            this.secondTeamDriverNameLabel.Text = "label5";
            this.secondTeamDriverNameLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // firstTeamDriverRankLabel
            // 
            this.firstTeamDriverRankLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.firstTeamDriverRankLabel.Location = new System.Drawing.Point(18, 252);
            this.firstTeamDriverRankLabel.Name = "firstTeamDriverRankLabel";
            this.firstTeamDriverRankLabel.Size = new System.Drawing.Size(203, 18);
            this.firstTeamDriverRankLabel.TabIndex = 2;
            this.firstTeamDriverRankLabel.Text = "label4";
            this.firstTeamDriverRankLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // firstTeamDriverNameLabel
            // 
            this.firstTeamDriverNameLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.firstTeamDriverNameLabel.Location = new System.Drawing.Point(18, 234);
            this.firstTeamDriverNameLabel.Name = "firstTeamDriverNameLabel";
            this.firstTeamDriverNameLabel.Size = new System.Drawing.Size(203, 18);
            this.firstTeamDriverNameLabel.TabIndex = 1;
            this.firstTeamDriverNameLabel.Text = "label3";
            this.firstTeamDriverNameLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // secondTeamDriverPictureBox
            // 
            this.secondTeamDriverPictureBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.secondTeamDriverPictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.secondTeamDriverPictureBox.Location = new System.Drawing.Point(252, 11);
            this.secondTeamDriverPictureBox.Name = "secondTeamDriverPictureBox";
            this.secondTeamDriverPictureBox.Size = new System.Drawing.Size(200, 220);
            this.secondTeamDriverPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.secondTeamDriverPictureBox.TabIndex = 16;
            this.secondTeamDriverPictureBox.TabStop = false;
            this.secondTeamDriverPictureBox.Click += new System.EventHandler(this.SecondTeamDriverPictureBox_Click);
            // 
            // firstTeamDriverPictureBox
            // 
            this.firstTeamDriverPictureBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.firstTeamDriverPictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.firstTeamDriverPictureBox.Location = new System.Drawing.Point(21, 11);
            this.firstTeamDriverPictureBox.Name = "firstTeamDriverPictureBox";
            this.firstTeamDriverPictureBox.Size = new System.Drawing.Size(200, 220);
            this.firstTeamDriverPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.firstTeamDriverPictureBox.TabIndex = 15;
            this.firstTeamDriverPictureBox.TabStop = false;
            this.firstTeamDriverPictureBox.Click += new System.EventHandler(this.FirstTeamDriverPictureBox_Click);
            // 
            // changeEmblemButton
            // 
            this.changeEmblemButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.changeEmblemButton.AutoSize = true;
            this.changeEmblemButton.BackColor = System.Drawing.Color.Black;
            this.changeEmblemButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.changeEmblemButton.ForeColor = System.Drawing.Color.White;
            this.changeEmblemButton.Location = new System.Drawing.Point(21, 316);
            this.changeEmblemButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.changeEmblemButton.Name = "changeEmblemButton";
            this.changeEmblemButton.Size = new System.Drawing.Size(149, 30);
            this.changeEmblemButton.TabIndex = 7;
            this.changeEmblemButton.Text = "CHANGE EMBLEM";
            this.changeEmblemButton.UseVisualStyleBackColor = false;
            this.changeEmblemButton.Click += new System.EventHandler(this.ChangeEmblemButton_Click);
            // 
            // countyFlagPictureBox
            // 
            this.countyFlagPictureBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.countyFlagPictureBox.Location = new System.Drawing.Point(222, 317);
            this.countyFlagPictureBox.Name = "countyFlagPictureBox";
            this.countyFlagPictureBox.Size = new System.Drawing.Size(25, 25);
            this.countyFlagPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.countyFlagPictureBox.TabIndex = 8;
            this.countyFlagPictureBox.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.teamEmblemPictureBox);
            this.groupBox3.Controls.Add(this.teamAliasLabel);
            this.groupBox3.Controls.Add(this.teamRankLabel);
            this.groupBox3.Controls.Add(this.teamPointsLabel);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox3.Location = new System.Drawing.Point(476, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(245, 352);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            // 
            // teamEmblemPictureBox
            // 
            this.teamEmblemPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.teamEmblemPictureBox.Location = new System.Drawing.Point(6, 19);
            this.teamEmblemPictureBox.Name = "teamEmblemPictureBox";
            this.teamEmblemPictureBox.Size = new System.Drawing.Size(233, 233);
            this.teamEmblemPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.teamEmblemPictureBox.TabIndex = 10;
            this.teamEmblemPictureBox.TabStop = false;
            // 
            // teamAliasLabel
            // 
            this.teamAliasLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.teamAliasLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.teamAliasLabel.Location = new System.Drawing.Point(6, 257);
            this.teamAliasLabel.Name = "teamAliasLabel";
            this.teamAliasLabel.Size = new System.Drawing.Size(233, 18);
            this.teamAliasLabel.TabIndex = 0;
            this.teamAliasLabel.Text = "Team";
            this.teamAliasLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // teamRankLabel
            // 
            this.teamRankLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.teamRankLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.teamRankLabel.Location = new System.Drawing.Point(6, 296);
            this.teamRankLabel.Name = "teamRankLabel";
            this.teamRankLabel.Size = new System.Drawing.Size(233, 18);
            this.teamRankLabel.TabIndex = 1;
            this.teamRankLabel.Text = "Rank";
            this.teamRankLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // teamPointsLabel
            // 
            this.teamPointsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.teamPointsLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.teamPointsLabel.Location = new System.Drawing.Point(6, 321);
            this.teamPointsLabel.Name = "teamPointsLabel";
            this.teamPointsLabel.Size = new System.Drawing.Size(233, 18);
            this.teamPointsLabel.TabIndex = 2;
            this.teamPointsLabel.Text = "Points";
            this.teamPointsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // teamBaseLabel
            // 
            this.teamBaseLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.teamBaseLabel.AutoSize = true;
            this.teamBaseLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.teamBaseLabel.Location = new System.Drawing.Point(191, 323);
            this.teamBaseLabel.Name = "teamBaseLabel";
            this.teamBaseLabel.Size = new System.Drawing.Size(31, 13);
            this.teamBaseLabel.TabIndex = 6;
            this.teamBaseLabel.Text = "Base";
            this.teamBaseLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // teamNameLabel
            // 
            this.teamNameLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.teamNameLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.teamNameLabel.Location = new System.Drawing.Point(21, 286);
            this.teamNameLabel.Name = "teamNameLabel";
            this.teamNameLabel.Size = new System.Drawing.Size(431, 18);
            this.teamNameLabel.TabIndex = 5;
            this.teamNameLabel.Text = "Name";
            this.teamNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // addTeamDriverButton
            // 
            this.addTeamDriverButton.AutoSize = true;
            this.addTeamDriverButton.BackColor = System.Drawing.Color.Black;
            this.addTeamDriverButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addTeamDriverButton.ForeColor = System.Drawing.Color.White;
            this.addTeamDriverButton.Location = new System.Drawing.Point(39, 326);
            this.addTeamDriverButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.addTeamDriverButton.Name = "addTeamDriverButton";
            this.addTeamDriverButton.Size = new System.Drawing.Size(140, 30);
            this.addTeamDriverButton.TabIndex = 4;
            this.addTeamDriverButton.Text = "ADD TEAM DRIVER";
            this.addTeamDriverButton.UseVisualStyleBackColor = false;
            this.addTeamDriverButton.Click += new System.EventHandler(this.AddTeamDriverButton_Click);
            // 
            // TeamForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(952, 364);
            this.Controls.Add(this.addTeamDriverButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.yearNumericUpDown);
            this.Controls.Add(this.teamListBox);
            this.Controls.Add(this.dataPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TeamForm";
            this.Text = "TeamForm";
            this.Load += new System.EventHandler(this.TeamForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.yearNumericUpDown)).EndInit();
            this.dataPanel.ResumeLayout(false);
            this.dataPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondTeamDriverPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstTeamDriverPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countyFlagPictureBox)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teamEmblemPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown yearNumericUpDown;
        private System.Windows.Forms.ListBox teamListBox;
        private System.Windows.Forms.Panel dataPanel;
        private System.Windows.Forms.Label secondTeamDriverRankLabel;
        private System.Windows.Forms.Label secondTeamDriverNameLabel;
        private System.Windows.Forms.Label firstTeamDriverRankLabel;
        private System.Windows.Forms.Label firstTeamDriverNameLabel;
        private System.Windows.Forms.PictureBox secondTeamDriverPictureBox;
        private System.Windows.Forms.PictureBox firstTeamDriverPictureBox;
        private System.Windows.Forms.Button changeEmblemButton;
        private System.Windows.Forms.PictureBox countyFlagPictureBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox teamEmblemPictureBox;
        private System.Windows.Forms.Label teamAliasLabel;
        private System.Windows.Forms.Label teamRankLabel;
        private System.Windows.Forms.Label teamPointsLabel;
        private System.Windows.Forms.Label teamBaseLabel;
        private System.Windows.Forms.Label teamNameLabel;
        private System.Windows.Forms.Button addTeamDriverButton;
    }
}