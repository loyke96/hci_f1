﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace FormulaOne
{
    public partial class AddCountryForm : Form
    {
        public AddCountryForm()
        {
            InitializeComponent();
        }

        private void SelectFlagButton_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Image Files|*.bmp;*.png;*.jpg;*.jpeg"
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    flagPictureBox.Image = Image.FromFile(openFileDialog.FileName);
                    selectLabel.Dispose();
                }
                catch (Exception) { }
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                var memoryStream = new MemoryStream();
                flagPictureBox.Image.Save(memoryStream, ImageFormat.Jpeg);
                var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "INSERT INTO COUNTRY VALUES(NULL, @NAME, @FLAG)";
                command.Parameters.AddWithValue("@NAME", nameTextBox.Text);
                command.Parameters.AddWithValue("@FLAG", memoryStream.ToArray());
                command.ExecuteNonQuery();
                connection.Close();
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception)
            {
                errorLabel.Show();
            }
        }
    }
}
