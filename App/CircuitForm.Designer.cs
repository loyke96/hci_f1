﻿namespace FormulaOne
{
    partial class CircuitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addCircuitButton = new System.Windows.Forms.Button();
            this.circuitListBox = new System.Windows.Forms.ListBox();
            this.dataPanel = new System.Windows.Forms.Panel();
            this.flagPictureBox = new System.Windows.Forms.PictureBox();
            this.nextRaceLabel = new System.Windows.Forms.Label();
            this.lastWinnerLabel = new System.Windows.Forms.Label();
            this.lastRaceLabel = new System.Windows.Forms.Label();
            this.locationLabel = new System.Windows.Forms.Label();
            this.layoutPictureBox = new System.Windows.Forms.PictureBox();
            this.circuitNameLabel = new System.Windows.Forms.Label();
            this.dataPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flagPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // addCircuitButton
            // 
            this.addCircuitButton.AutoSize = true;
            this.addCircuitButton.BackColor = System.Drawing.Color.Black;
            this.addCircuitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addCircuitButton.ForeColor = System.Drawing.Color.White;
            this.addCircuitButton.Location = new System.Drawing.Point(39, 319);
            this.addCircuitButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.addCircuitButton.Name = "addCircuitButton";
            this.addCircuitButton.Size = new System.Drawing.Size(140, 30);
            this.addCircuitButton.TabIndex = 2;
            this.addCircuitButton.Text = "ADD CIRCUIT";
            this.addCircuitButton.UseVisualStyleBackColor = false;
            this.addCircuitButton.Click += new System.EventHandler(this.AddCircuitButton_Click);
            // 
            // circuitListBox
            // 
            this.circuitListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.circuitListBox.DisplayMember = "Name";
            this.circuitListBox.ForeColor = System.Drawing.Color.Black;
            this.circuitListBox.FormattingEnabled = true;
            this.circuitListBox.Location = new System.Drawing.Point(4, 6);
            this.circuitListBox.Name = "circuitListBox";
            this.circuitListBox.Size = new System.Drawing.Size(211, 288);
            this.circuitListBox.TabIndex = 0;
            this.circuitListBox.ValueMember = "ID";
            this.circuitListBox.SelectedIndexChanged += new System.EventHandler(this.CircuitListBox_SelectedIndexChanged);
            // 
            // dataPanel
            // 
            this.dataPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataPanel.Controls.Add(this.flagPictureBox);
            this.dataPanel.Controls.Add(this.nextRaceLabel);
            this.dataPanel.Controls.Add(this.lastWinnerLabel);
            this.dataPanel.Controls.Add(this.lastRaceLabel);
            this.dataPanel.Controls.Add(this.locationLabel);
            this.dataPanel.Controls.Add(this.layoutPictureBox);
            this.dataPanel.Controls.Add(this.circuitNameLabel);
            this.dataPanel.Location = new System.Drawing.Point(219, 3);
            this.dataPanel.Name = "dataPanel";
            this.dataPanel.Size = new System.Drawing.Size(729, 358);
            this.dataPanel.TabIndex = 1;
            // 
            // flagPictureBox
            // 
            this.flagPictureBox.Location = new System.Drawing.Point(19, 146);
            this.flagPictureBox.Name = "flagPictureBox";
            this.flagPictureBox.Size = new System.Drawing.Size(200, 200);
            this.flagPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.flagPictureBox.TabIndex = 25;
            this.flagPictureBox.TabStop = false;
            // 
            // nextRaceLabel
            // 
            this.nextRaceLabel.AutoSize = true;
            this.nextRaceLabel.Location = new System.Drawing.Point(16, 120);
            this.nextRaceLabel.Name = "nextRaceLabel";
            this.nextRaceLabel.Size = new System.Drawing.Size(56, 13);
            this.nextRaceLabel.TabIndex = 3;
            this.nextRaceLabel.Text = "Next race:";
            // 
            // lastWinnerLabel
            // 
            this.lastWinnerLabel.AutoSize = true;
            this.lastWinnerLabel.Location = new System.Drawing.Point(16, 83);
            this.lastWinnerLabel.Name = "lastWinnerLabel";
            this.lastWinnerLabel.Size = new System.Drawing.Size(64, 13);
            this.lastWinnerLabel.TabIndex = 2;
            this.lastWinnerLabel.Text = "Last winner:";
            // 
            // lastRaceLabel
            // 
            this.lastRaceLabel.AutoSize = true;
            this.lastRaceLabel.Location = new System.Drawing.Point(16, 46);
            this.lastRaceLabel.Name = "lastRaceLabel";
            this.lastRaceLabel.Size = new System.Drawing.Size(54, 13);
            this.lastRaceLabel.TabIndex = 1;
            this.lastRaceLabel.Text = "Last race:";
            // 
            // locationLabel
            // 
            this.locationLabel.AutoSize = true;
            this.locationLabel.Location = new System.Drawing.Point(16, 9);
            this.locationLabel.Name = "locationLabel";
            this.locationLabel.Size = new System.Drawing.Size(51, 13);
            this.locationLabel.TabIndex = 0;
            this.locationLabel.Text = "Location:";
            // 
            // layoutPictureBox
            // 
            this.layoutPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutPictureBox.Location = new System.Drawing.Point(241, 9);
            this.layoutPictureBox.Name = "layoutPictureBox";
            this.layoutPictureBox.Size = new System.Drawing.Size(480, 300);
            this.layoutPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.layoutPictureBox.TabIndex = 2;
            this.layoutPictureBox.TabStop = false;
            // 
            // circuitNameLabel
            // 
            this.circuitNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.circuitNameLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.circuitNameLabel.Location = new System.Drawing.Point(241, 316);
            this.circuitNameLabel.Name = "circuitNameLabel";
            this.circuitNameLabel.Size = new System.Drawing.Size(480, 18);
            this.circuitNameLabel.TabIndex = 4;
            this.circuitNameLabel.Text = "Name";
            this.circuitNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CircuitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(952, 364);
            this.Controls.Add(this.addCircuitButton);
            this.Controls.Add(this.circuitListBox);
            this.Controls.Add(this.dataPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CircuitForm";
            this.Text = "DriverForm";
            this.Load += new System.EventHandler(this.CircuitForm_Load);
            this.dataPanel.ResumeLayout(false);
            this.dataPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flagPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addCircuitButton;
        private System.Windows.Forms.ListBox circuitListBox;
        private System.Windows.Forms.Panel dataPanel;
        private System.Windows.Forms.PictureBox layoutPictureBox;
        private System.Windows.Forms.Label circuitNameLabel;
        private System.Windows.Forms.Label lastRaceLabel;
        private System.Windows.Forms.Label locationLabel;
        private System.Windows.Forms.Label nextRaceLabel;
        private System.Windows.Forms.Label lastWinnerLabel;
        private System.Windows.Forms.PictureBox flagPictureBox;
    }
}