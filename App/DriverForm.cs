﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlTypes;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace FormulaOne
{
    public partial class DriverForm : Form
    {
        private object team;

        public DriverForm()
        {
            InitializeComponent();
            yearNumericUpDown.Value = DateTime.Today.Year;
        }

        public DriverForm(int year, object driver)
        {
            InitializeComponent();
            yearNumericUpDown.Value = year;
            driverListBox.SelectedItem = driver;
        }

        private void DriverForm_Load(object sender, EventArgs e)
        {
            yearNumericUpDown.Maximum = DateTime.Today.Year;
        }

        private void DriverYearNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
            connection.Open();
            var previusSelected = driverListBox.SelectedItem;
            var command = connection.CreateCommand();
            command.CommandText = "SELECT ID, CONCAT(`FIRST NAME`, ' ', `LAST NAME`) FROM DRIVER JOIN `TEAM DRIVER` ON ID = DRIVER_ID WHERE SEASON = @SEASON";
            command.Parameters.AddWithValue("@SEASON", yearNumericUpDown.Value);
            var reader = command.ExecuteReader();
            var drivers = new List<object>();
            while (reader.Read())
            {
                drivers.Add(new
                {
                    ID = reader.GetInt32(0),
                    Name = reader.GetString(1)
                });
            }
            reader.Close();
            driverListBox.DataSource = drivers;
            driverListBox.ValueMember = "ID";
            driverListBox.DisplayMember = "Name";
            if (drivers.Contains(previusSelected))
                driverListBox.SelectedItem = previusSelected;
            if (drivers.Count == 0)
                dataPanel.Hide();
            connection.Close();
        }

        private void DriverListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (driverListBox.SelectedItem == null)
            {
                dataPanel.Hide();
            }
            else
            {
                var buffer = new byte[1_024 * 1_024];
                var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM DRIVER_DATA WHERE SEASON = @SEASON AND ID = @ID";
                command.Parameters.AddWithValue("@SEASON", yearNumericUpDown.Value);
                command.Parameters.AddWithValue("@ID", driverListBox.SelectedValue);
                var reader = command.ExecuteReader();
                reader.Read();
                driverNameLabel.Text = reader.GetString(1);
                driverCountryLabel.Text = $"Nationality:         {reader.GetString(4)}";
                driverBornLabel.Text = $"Born:  {reader.GetDateTime(2).ToString("dd. MM. yyyy.")}";
                rankingLabel.Text = $"Rank:  {reader.GetString(6)}";
                pointsLabel.Text = $"Points:  {reader.GetString(7)}";
                teamLabel.Text = $"Team:  {reader.GetString(8)}";
                reader.GetBytes(5, 0, buffer, 0, buffer.Length);
                flagPictureBox.Image = Image.FromStream(new MemoryStream(buffer));
                reader.GetBytes(3, 0, buffer, 0, buffer.Length);
                driverPictureBox.Image = Image.FromStream(new MemoryStream(buffer));
                reader.GetBytes(10, 0, buffer, 0, buffer.Length);
                emblemPictureBox.Image = Image.FromStream(new MemoryStream(buffer));
                team = new
                {
                    ID = reader.GetInt32(9),
                    Name = reader.GetString(8)
                };
                reader.Close();

                command.CommandText = "SELECT MIN(`RANK`) FROM RANKING_HISTORY WHERE DRIVER_ID = @ID";
                careerBestRankLabel.Text = $"Best rank:  {command.ExecuteScalar()}";
                command.CommandText = "SELECT COUNT(*) FROM RACE_RANKS WHERE DRIVER_ID = @ID AND `RANK` <= 3";
                careerPodiumsLabel.Text = $"Podiums:  {command.ExecuteScalar()}";
                command.CommandText = "SELECT COUNT(*) FROM RACE_RANKS WHERE DRIVER_ID = @ID AND `RANK` <= 3 AND SEASON = @SEASON";
                seasonPodiumsLabel.Text = $"Podiums:  {command.ExecuteScalar()}";
                command.CommandText = "SELECT MIN(`RANK`) FROM RACE_RANKS WHERE DRIVER_ID = @ID AND SEASON = @SEASON";
                var bestFinish = command.ExecuteScalar();
                seasonBestFinishLabel.Text = $"Best finish:  {(bestFinish == DBNull.Value ? "N/A" : bestFinish)}";

                command.CommandText = "SELECT MAX(`DATE`), `RANK` FROM RACE_RANKS WHERE DRIVER_ID = @ID";
                try
                {
                    reader = command.ExecuteReader();
                    reader.Read();
                    lastRaceLabel.Text = $"Last race:  {reader.GetDateTime(0).ToString("dd. MM. yyyy.")}";
                    lastRaceRankLabel.Text = $"Last race rank:  {reader.GetString(1)}";
                }
                catch (SqlNullValueException)
                {
                    lastRaceLabel.Text = $"Last race:  N/A";
                    lastRaceRankLabel.Text = $"Last race rank:  N/A";
                }
                finally
                {
                    reader.Close();
                }

                command.CommandText = "SELECT MIN(`RANK`), COUNT(*) FROM RACE_RANKS WHERE DRIVER_ID = @ID";
                reader = command.ExecuteReader();
                reader.Read();
                careerBestFinishLabel.Text = $"Best finish:  {(reader.IsDBNull(0) ? "N/A" : reader.GetString(0))}";
                grandPrixEnteredLabel.Text = $"Grand Prix entered:  {(reader.IsDBNull(1) ? "N/A" : reader.GetString(1))}";
                reader.Close();

                dataPanel.Show();
                connection.Close();
            }
        }

        private void AddTeamDriverButton_Click(object sender, EventArgs e)
        {
            if (new AddTeamDriverForm
            {
                Season = (int)yearNumericUpDown.Value
            }.ShowDialog() == DialogResult.OK)
            {
                DriverYearNumericUpDown_ValueChanged(null, null);
            }
        }

        private void ChangePhotoButton_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Image Files|*.bmp;*.png;*.jpg;*.jpeg"
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                var memoryStream = new MemoryStream();
                var photo = Image.FromFile(openFileDialog.FileName);
                photo.Save(memoryStream, ImageFormat.Jpeg);
                var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "UPDATE DRIVER SET PICTURE = @PICTURE WHERE ID = @ID";
                command.Parameters.AddWithValue("@PICTURE", memoryStream.ToArray());
                command.Parameters.AddWithValue("@ID", driverListBox.SelectedValue);
                command.ExecuteNonQuery();
                connection.Close();
                driverPictureBox.Image = photo;
            }
        }

        private void EmblemPictureBox_Click(object sender, EventArgs e)
        {
            Controls.Clear();
            var teamForm = new TeamForm((int)yearNumericUpDown.Value, team)
            {
                TopLevel = false
            };
            Controls.Add(teamForm);
            teamForm.Show();
        }
    }
}
