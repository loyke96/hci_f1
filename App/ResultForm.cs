﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace FormulaOne
{
    public partial class ResultForm : Form
    {
        public ResultForm()
        {
            InitializeComponent();
        }

        private void ResultForm_Load(object sender, EventArgs e)
        {
            yearNumericUpDown.Value
                = yearNumericUpDown.Maximum
                = DateTime.Today.Year;
        }

        private void YearNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
            connection.Open();
            var previusSelected = grandPrixListBox.SelectedItem;
            var command = connection.CreateCommand();
            command.CommandText = "SELECT DATE, TITLE FROM RACE NATURAL JOIN `GRAND PRIX` WHERE SEASON = @SEASON";
            command.Parameters.AddWithValue("@SEASON", yearNumericUpDown.Value);
            var reader = command.ExecuteReader();
            var grandPrix = new List<object>();
            while (reader.Read())
            {
                grandPrix.Add(new
                {
                    ID = reader.GetDateTime(0),
                    Name = reader.GetString(1)
                });
            }
            reader.Close();
            grandPrixListBox.DataSource = grandPrix;
            grandPrixListBox.ValueMember = "ID";
            grandPrixListBox.DisplayMember = "Name";
            if (grandPrix.Contains(previusSelected))
                grandPrixListBox.SelectedItem = previusSelected;
            if (grandPrix.Count == 0)
                dataPanel.Hide();
            else
            {
                command.CommandText = "SELECT `RANK`, `NAME`, POINTS FROM TEAM_DRIVERS WHERE SEASON = @SEASON";
                DataTable dataTable = new DataTable();
                new MySqlDataAdapter(command).Fill(dataTable);
                driverDataGridView.DataSource = dataTable;

                command.CommandText = "SELECT `RANK`, ALIAS, POINTS FROM TEAM_POINTS WHERE SEASON = @SEASON";
                dataTable = new DataTable();
                new MySqlDataAdapter(command).Fill(dataTable);
                teamDataGridView.DataSource = dataTable;
            }
            connection.Close();
        }

        private void GrandPrixListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (grandPrixListBox.SelectedItem == null)
            {
                dataPanel.Hide();
            }
            else
            {
                var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "SELECT `RANK`, DRIVER, `TIME` FROM RACE_RESULTS WHERE DATE = @DATE";
                command.Parameters.AddWithValue("@DATE", grandPrixListBox.SelectedValue);
                DataTable dataTable = new DataTable();
                new MySqlDataAdapter(command).Fill(dataTable);
                connection.Close();
                raceDataGridView.DataSource = dataTable;
                if (dataTable.Rows.Count == 0)
                {
                    dataTable.Rows.Add(new object[] { null, "Results Currently Unavailable", null });
                    raceDataGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                }
                dataPanel.Show();
            }
        }

        private void DataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex == 0)
            {
                e.CellStyle.BackColor = Color.Red;
                e.CellStyle.SelectionBackColor = Color.Red;
                e.CellStyle.ForeColor = Color.White;
                e.CellStyle.SelectionForeColor = Color.White;
            }
        }
    }
}
