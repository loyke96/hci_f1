﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace FormulaOne
{
    public partial class RaceForm : Form
    {
        private DateTime lastRaceDate;
        public RaceForm()
        {
            InitializeComponent();
        }

        private void RaceForm_Load(object sender, EventArgs e)
        {
            var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM `GRAND PRIX`";
            var reader = command.ExecuteReader();
            var races = new List<object>();
            while (reader.Read())
            {
                races.Add(new
                {
                    ID = reader.GetInt32(0),
                    Name = reader.GetString(1)
                });
            }
            reader.Close();
            raceListBox.DataSource = races;
            raceListBox.ValueMember = "ID";
            raceListBox.DisplayMember = "Name";
            connection.Close();
        }

        private void RaceListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (raceListBox.SelectedItem == null)
            {
                dataPanel.Hide();
            }
            else
            {
                var buffer = new byte[1_024 * 1_024];
                var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM RACE_DETAILS WHERE COUNTRY_ID = @ID ORDER BY `DATE` DESC LIMIT 1";
                command.Parameters.AddWithValue("@ID", raceListBox.SelectedValue);
                var reader = command.ExecuteReader();
                if (reader.Read())
                {
                    lastRaceDate = reader.GetDateTime(2);
                    dateLabel.Text = $"Date: {lastRaceDate.ToString("dd. MM. yyyy.")}";
                    locationLabel.Text = $"Location: {reader.GetString(3)}";
                    reader.Close();

                    DataTable dataTable = new DataTable();
                    command.CommandText = "SELECT * FROM RACE_RESULTS WHERE `DATE` = @DATE";
                    command.Parameters.AddWithValue("@DATE", lastRaceDate);
                    new MySqlDataAdapter(command).Fill(dataTable);
                    resultDataGridView.DataSource = dataTable;
                    resultDataGridView.Columns[3].Visible = false;
                    resultDataGridView.Show();

                    if (resultDataGridView.Rows.Count == 0)
                    {
                        addResultsButton.Show();
                    }
                    else
                    {
                        addResultsButton.Hide();
                    }
                }
                else
                {
                    resultDataGridView.Hide();
                    addResultsButton.Hide();
                    dateLabel.Text = $"Date: N/A";
                    locationLabel.Text = $"Location: N/A";
                    reader.Close();
                }

                command.CommandText = "SELECT * FROM NEXT_RACE WHERE COUNTRY_ID = @ID ORDER BY `DATE` LIMIT 1";
                reader = command.ExecuteReader();
                if (reader.Read())
                {
                    nextDateLabel.Text = $"Date: {reader.GetDateTime(1).ToString("dd. MM. yyyy.")}";
                    nextLocationLabel.Text = $"Location: {reader.GetString(2)}, {reader.GetString(4)}";
                    reader.GetBytes(3, 0, buffer, 0, buffer.Length);
                    layoutPictureBox.Image = Image.FromStream(new MemoryStream(buffer));
                    scheduleButton.Hide();
                }
                else
                {
                    nextDateLabel.Text = "Date: N/A";
                    nextLocationLabel.Text = "Location: N/A";
                    layoutPictureBox.Image = null;
                    scheduleButton.Show();
                }
                reader.Close();

                dataPanel.Show();
                connection.Close();
            }
        }

        private void AddGPButton_Click(object sender, EventArgs e)
        {
            if (new AddGrandPrixForm().ShowDialog() == DialogResult.OK)
            {
                RaceForm_Load(null, null);
            }
        }

        private void ScheduleButton_Click(object sender, EventArgs e)
        {
            if (new ScheduleRaceForm((int)raceListBox.SelectedValue).ShowDialog() == DialogResult.OK)
            {
                RaceListBox_SelectedIndexChanged(null, null);
            }
        }

        private void AddResultsButton_Click(object sender, EventArgs e)
        {
            if (new AddResultsForm((int)raceListBox.SelectedValue, lastRaceDate.Year).ShowDialog() == DialogResult.OK)
            {
                RaceListBox_SelectedIndexChanged(null, null);
            }
        }

        private void DataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex == 0)
            {
                e.CellStyle.BackColor = Color.Red;
                e.CellStyle.SelectionBackColor = Color.Red;
                e.CellStyle.ForeColor = Color.White;
                e.CellStyle.SelectionForeColor = Color.White;
            }
        }
    }
}
