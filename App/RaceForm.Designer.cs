﻿namespace FormulaOne {
    partial class RaceForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.addGPButton = new System.Windows.Forms.Button();
            this.raceListBox = new System.Windows.Forms.ListBox();
            this.dataPanel = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.scheduleButton = new System.Windows.Forms.Button();
            this.layoutPictureBox = new System.Windows.Forms.PictureBox();
            this.nextLocationLabel = new System.Windows.Forms.Label();
            this.nextDateLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.addResultsButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.resultDataGridView = new System.Windows.Forms.DataGridView();
            this.locationLabel = new System.Windows.Forms.Label();
            this.dateLabel = new System.Windows.Forms.Label();
            this.dataPanel.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPictureBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // addGPButton
            // 
            this.addGPButton.AutoSize = true;
            this.addGPButton.BackColor = System.Drawing.Color.Black;
            this.addGPButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addGPButton.ForeColor = System.Drawing.Color.White;
            this.addGPButton.Location = new System.Drawing.Point(39, 313);
            this.addGPButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.addGPButton.Name = "addGPButton";
            this.addGPButton.Size = new System.Drawing.Size(140, 30);
            this.addGPButton.TabIndex = 2;
            this.addGPButton.Text = "ADD GRAND PRIX";
            this.addGPButton.UseVisualStyleBackColor = false;
            this.addGPButton.Click += new System.EventHandler(this.AddGPButton_Click);
            // 
            // raceListBox
            // 
            this.raceListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.raceListBox.DisplayMember = "Name";
            this.raceListBox.ForeColor = System.Drawing.Color.Black;
            this.raceListBox.FormattingEnabled = true;
            this.raceListBox.Location = new System.Drawing.Point(4, 6);
            this.raceListBox.Name = "raceListBox";
            this.raceListBox.Size = new System.Drawing.Size(211, 288);
            this.raceListBox.TabIndex = 0;
            this.raceListBox.ValueMember = "ID";
            this.raceListBox.SelectedIndexChanged += new System.EventHandler(this.RaceListBox_SelectedIndexChanged);
            // 
            // dataPanel
            // 
            this.dataPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataPanel.Controls.Add(this.groupBox2);
            this.dataPanel.Controls.Add(this.groupBox1);
            this.dataPanel.Location = new System.Drawing.Point(219, 3);
            this.dataPanel.Name = "dataPanel";
            this.dataPanel.Size = new System.Drawing.Size(729, 358);
            this.dataPanel.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.scheduleButton);
            this.groupBox2.Controls.Add(this.layoutPictureBox);
            this.groupBox2.Controls.Add(this.nextLocationLabel);
            this.groupBox2.Controls.Add(this.nextDateLabel);
            this.groupBox2.Location = new System.Drawing.Point(365, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(356, 340);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Next race";
            // 
            // scheduleButton
            // 
            this.scheduleButton.AutoSize = true;
            this.scheduleButton.BackColor = System.Drawing.Color.Black;
            this.scheduleButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.scheduleButton.ForeColor = System.Drawing.Color.White;
            this.scheduleButton.Location = new System.Drawing.Point(14, 301);
            this.scheduleButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.scheduleButton.Name = "scheduleButton";
            this.scheduleButton.Size = new System.Drawing.Size(147, 30);
            this.scheduleButton.TabIndex = 2;
            this.scheduleButton.Text = "SCHEDULE";
            this.scheduleButton.UseVisualStyleBackColor = false;
            this.scheduleButton.Visible = false;
            this.scheduleButton.Click += new System.EventHandler(this.ScheduleButton_Click);
            // 
            // layoutPictureBox
            // 
            this.layoutPictureBox.Location = new System.Drawing.Point(14, 110);
            this.layoutPictureBox.Name = "layoutPictureBox";
            this.layoutPictureBox.Size = new System.Drawing.Size(327, 224);
            this.layoutPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.layoutPictureBox.TabIndex = 4;
            this.layoutPictureBox.TabStop = false;
            // 
            // nextLocationLabel
            // 
            this.nextLocationLabel.AutoSize = true;
            this.nextLocationLabel.Location = new System.Drawing.Point(11, 63);
            this.nextLocationLabel.Name = "nextLocationLabel";
            this.nextLocationLabel.Size = new System.Drawing.Size(51, 13);
            this.nextLocationLabel.TabIndex = 1;
            this.nextLocationLabel.Text = "Location:";
            // 
            // nextDateLabel
            // 
            this.nextDateLabel.AutoSize = true;
            this.nextDateLabel.Location = new System.Drawing.Point(11, 32);
            this.nextDateLabel.Name = "nextDateLabel";
            this.nextDateLabel.Size = new System.Drawing.Size(33, 13);
            this.nextDateLabel.TabIndex = 0;
            this.nextDateLabel.Text = "Date:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.addResultsButton);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.resultDataGridView);
            this.groupBox1.Controls.Add(this.locationLabel);
            this.groupBox1.Controls.Add(this.dateLabel);
            this.groupBox1.Location = new System.Drawing.Point(3, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(356, 340);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Last race";
            // 
            // addResultsButton
            // 
            this.addResultsButton.AutoSize = true;
            this.addResultsButton.BackColor = System.Drawing.Color.Black;
            this.addResultsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addResultsButton.ForeColor = System.Drawing.Color.White;
            this.addResultsButton.Location = new System.Drawing.Point(14, 301);
            this.addResultsButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.addResultsButton.Name = "addResultsButton";
            this.addResultsButton.Size = new System.Drawing.Size(147, 30);
            this.addResultsButton.TabIndex = 4;
            this.addResultsButton.Text = "ADD RESULTS";
            this.addResultsButton.UseVisualStyleBackColor = false;
            this.addResultsButton.Visible = false;
            this.addResultsButton.Click += new System.EventHandler(this.AddResultsButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Results:";
            // 
            // resultDataGridView
            // 
            this.resultDataGridView.AllowUserToAddRows = false;
            this.resultDataGridView.AllowUserToDeleteRows = false;
            this.resultDataGridView.AllowUserToResizeColumns = false;
            this.resultDataGridView.AllowUserToResizeRows = false;
            this.resultDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.resultDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.resultDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.resultDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            this.resultDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultDataGridView.ColumnHeadersVisible = false;
            this.resultDataGridView.Location = new System.Drawing.Point(6, 110);
            this.resultDataGridView.Name = "resultDataGridView";
            this.resultDataGridView.ReadOnly = true;
            this.resultDataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.resultDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.resultDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.resultDataGridView.Size = new System.Drawing.Size(344, 224);
            this.resultDataGridView.TabIndex = 3;
            this.resultDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DataGridView_CellFormatting);
            // 
            // locationLabel
            // 
            this.locationLabel.AutoSize = true;
            this.locationLabel.Location = new System.Drawing.Point(11, 63);
            this.locationLabel.Name = "locationLabel";
            this.locationLabel.Size = new System.Drawing.Size(51, 13);
            this.locationLabel.TabIndex = 1;
            this.locationLabel.Text = "Location:";
            // 
            // dateLabel
            // 
            this.dateLabel.AutoSize = true;
            this.dateLabel.Location = new System.Drawing.Point(11, 32);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(33, 13);
            this.dateLabel.TabIndex = 0;
            this.dateLabel.Text = "Date:";
            // 
            // RaceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(952, 364);
            this.Controls.Add(this.addGPButton);
            this.Controls.Add(this.raceListBox);
            this.Controls.Add(this.dataPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RaceForm";
            this.Text = "DriverForm";
            this.Load += new System.EventHandler(this.RaceForm_Load);
            this.dataPanel.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPictureBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addGPButton;
        private System.Windows.Forms.ListBox raceListBox;
        private System.Windows.Forms.Panel dataPanel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView resultDataGridView;
        private System.Windows.Forms.Label locationLabel;
        private System.Windows.Forms.Label dateLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox layoutPictureBox;
        private System.Windows.Forms.Label nextLocationLabel;
        private System.Windows.Forms.Label nextDateLabel;
        private System.Windows.Forms.Button scheduleButton;
        private System.Windows.Forms.Button addResultsButton;
    }
}