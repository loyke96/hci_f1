﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;

namespace FormulaOne
{
    public partial class ScheduleRaceForm : Form
    {
        private readonly int countryId;
        public ScheduleRaceForm(int countryId)
        {
            InitializeComponent();
            this.countryId = countryId;
        }


        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "INSERT INTO RACE VALUES(@COUNTRY, @SEASON, @CIRCUIT, @DATE, @TIME)";
                command.Parameters.AddWithValue("@COUNTRY", countryId);
                command.Parameters.AddWithValue("@SEASON", datePicker.Value.Year);
                command.Parameters.AddWithValue("@CIRCUIT", locationComboBox.SelectedValue);
                command.Parameters.AddWithValue("@DATE", datePicker.Value);
                command.Parameters.AddWithValue("@TIME", timePicker.Value);
                command.ExecuteNonQuery();
                connection.Close();
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception)
            {
                errorLabel.Text = "Check that all fields are filled in correctly and try again";
                errorLabel.Show();
            }
        }

        private void ScheduleRaceForm_Load(object sender, EventArgs e)
        {
            var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT ID, CONCAT(NAME, ', ', LOCATION) FROM CIRCUIT WHERE COUNTRY_ID = @ID";
            command.Parameters.AddWithValue("@ID", countryId);
            var reader = command.ExecuteReader();
            List<object> circuits = new List<object>();
            while (reader.Read())
            {
                circuits.Add(new
                {
                    ID = reader.GetInt32(0),
                    Name = reader.GetString(1)
                });
            }
            reader.Close();
            locationComboBox.DataSource = circuits;
            locationComboBox.ValueMember = "ID";
            locationComboBox.DisplayMember = "Name";
        }
    }
}
