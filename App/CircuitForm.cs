﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace FormulaOne
{
    public partial class CircuitForm : Form
    {
        public CircuitForm()
        {
            InitializeComponent();
        }

        private void CircuitForm_Load(object sender, EventArgs e)
        {
            var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT ID, NAME FROM CIRCUIT";
            var reader = command.ExecuteReader();
            var circuits = new List<object>();
            while (reader.Read())
            {
                circuits.Add(new
                {
                    ID = reader.GetInt32(0),
                    Name = reader.GetString(1)
                });
            }
            reader.Close();
            circuitListBox.DataSource = circuits;
            circuitListBox.ValueMember = "ID";
            circuitListBox.DisplayMember = "Name";
            connection.Close();
        }

        private void CircuitListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (circuitListBox.SelectedItem == null)
            {
                dataPanel.Hide();
            }
            else
            {
                var buffer = new byte[1_024 * 1_024];
                var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM CIRCUIT CT JOIN COUNTRY CY ON CY.ID = CT.COUNTRY_ID WHERE CT.ID = @ID";
                command.Parameters.AddWithValue("@ID", circuitListBox.SelectedValue);
                var reader = command.ExecuteReader();
                if (reader.Read())
                {
                    circuitNameLabel.Text = reader.GetString(1);
                    locationLabel.Text = $"Location: {reader.GetString(3)}, {reader.GetString(6)}";
                    reader.GetBytes(2, 0, buffer, 0, buffer.Length);
                    layoutPictureBox.Image = Image.FromStream(new MemoryStream(buffer));
                    reader.GetBytes(7, 0, buffer, 0, buffer.Length);
                    flagPictureBox.Image = Image.FromStream(new MemoryStream(buffer));
                }
                reader.Close();

                command.CommandText = "SELECT MAX(`DATE`) FROM RACE WHERE DATE < CURRENT_DATE() AND CIRCUIT_ID = @ID";
                try
                {
                    reader = command.ExecuteReader();
                    reader.Read();
                    lastRaceLabel.Text = $"Last race: {reader.GetDateTime(0).ToString("dd. MM. yyyy.")}";
                }
                catch (SqlNullValueException)
                {
                    lastRaceLabel.Text = "Last race: N/A";
                }
                finally { reader.Close(); }

                command.CommandText = "SELECT MIN(`DATE`) FROM RACE WHERE DATE >= CURRENT_DATE() AND CIRCUIT_ID = @ID";
                try
                {
                    reader = command.ExecuteReader();
                    reader.Read();
                    nextRaceLabel.Text = $"Next race: {reader.GetDateTime(0).ToString("dd. MM. yyyy.")}";
                }
                catch (SqlNullValueException)
                {
                    nextRaceLabel.Text = "Next race: N/A";
                }
                finally { reader.Close(); }

                command.CommandText = "SELECT " +
                    "CONCAT(`FIRST NAME`, ' ', `LAST NAME`) " +
                    "FROM RACE NATURAL JOIN " +
                    "`DRIVER RACE` JOIN " +
                    "DRIVER ON ID = DRIVER_ID " +
                    "WHERE CIRCUIT_ID = @ID " +
                    "ORDER BY DATE DESC, TIME " +
                    "LIMIT 1";

                reader = command.ExecuteReader();
                if (reader.Read())
                    lastWinnerLabel.Text = $"Last winner: {reader.GetString(0)}";
                else
                    lastWinnerLabel.Text = "Last winner: N/A";
                reader.Close();

                dataPanel.Show();
                connection.Close();
            }
        }

        private void AddCircuitButton_Click(object sender, EventArgs e)
        {
            if (new AddCircuitForm().ShowDialog() == DialogResult.OK)
            {
                CircuitForm_Load(null, null);
            }
        }
    }
}
