﻿using MySql.Data.MySqlClient;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace FormulaOne
{
    public partial class AddResultsForm : Form
    {
        private bool close = false;
        private int[] scoring = { 25, 18, 15, 12, 10, 8, 6, 4, 2, 1 };
        private readonly int countryId;
        private readonly int season;

        public AddResultsForm(int countryId, int season)
        {
            InitializeComponent();
            this.countryId = countryId;
            this.season = season;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure all given information is correct", "Sure?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "INSERT INTO `DRIVER RACE` VALUES(@DRIVER, @COUNTRY, @SEASON, @TIME)";
                command.Parameters.AddWithValue("@COUNTRY", countryId);
                command.Parameters.AddWithValue("@SEASON", season);
                command.Parameters.AddWithValue("@DRIVER", null);
                command.Parameters.AddWithValue("@TIME", null);
                command.Parameters.AddWithValue("@SCORE", null);
                foreach (DataGridViewRow row in resultDataGridView.Rows)
                {
                    command.Parameters["@DRIVER"].Value = row.Cells[0].Value;
                    command.Parameters["@TIME"].Value = row.Cells[2].Value;
                    command.ExecuteNonQuery();
                }
                command.CommandText = "UPDATE `TEAM DRIVER` SET POINTS = POINTS + @SCORE WHERE DRIVER_ID = @DRIVER AND SEASON = @SEASON";
                int driversWithPoints = resultDataGridView.Rows.Count;
                if (driversWithPoints > 10) driversWithPoints = 10;
                foreach (int i in Enumerable.Range(0, driversWithPoints))
                {
                    command.Parameters["@SCORE"].Value = scoring[i];
                    command.Parameters["@DRIVER"].Value = resultDataGridView[0, i].Value;
                    command.ExecuteNonQuery();
                }

                connection.Close();
                close = true;
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void AddResultsForm_Load(object sender, EventArgs e)
        {
            var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT ID, CONCAT(`FIRST NAME`, ' ', `LAST NAME`) AS DRIVER, TIME('2:00') AS TIME " +
                "FROM DRIVER JOIN `TEAM DRIVER` ON ID = DRIVER_ID WHERE SEASON = @SEASON";
            command.Parameters.AddWithValue("@SEASON", season);
            DataTable dataTable = new DataTable();
            new MySqlDataAdapter(command).Fill(dataTable);
            resultDataGridView.DataSource = dataTable;
            resultDataGridView.Columns[0].Visible = false;
            resultDataGridView.Columns[1].ReadOnly = true;
            resultDataGridView.Columns[2].Width = 120;
        }

        private void AddResultsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (close == false && MessageBox.Show("If you click Yes, you will lose any unsaved work. " +
                "Are you sure you want to discard?", "Sure?", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void ResultDataGridView_Leave(object sender, EventArgs e)
        {
            resultDataGridView.Sort(resultDataGridView.Columns[2], ListSortDirection.Ascending);
        }

        private void ResultDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }
    }
}
