﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;

namespace FormulaOne
{
    public partial class AddGrandPrixForm : Form
    {
        public AddGrandPrixForm()
        {
            InitializeComponent();
        }


        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "INSERT INTO `GRAND PRIX` VALUES(@COUNTRY, @NAME)";
                command.Parameters.AddWithValue("@COUNTRY", countryComboBox.SelectedValue);
                command.Parameters.AddWithValue("@NAME", gpNameTextBox.Text);
                command.ExecuteNonQuery();
                connection.Close();
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception)
            {
                errorLabel.Text = "Check that all fields are filled in correctly and try again";
                errorLabel.Show();
            }
        }

        private void ScheduleRaceForm_Load(object sender, EventArgs e)
        {
            var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM NON_GP_COUNTRIES";
            var reader = command.ExecuteReader();
            List<object> countries = new List<object>();
            while (reader.Read())
            {
                countries.Add(new
                {
                    ID = reader.GetInt32(0),
                    Name = reader.GetString(1)
                });
            }
            reader.Close();
            countryComboBox.DataSource = countries;
            countryComboBox.ValueMember = "ID";
            countryComboBox.DisplayMember = "Name";
        }
    }
}
