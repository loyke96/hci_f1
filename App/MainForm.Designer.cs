﻿namespace FormulaOne
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.driverButton = new System.Windows.Forms.Button();
            this.teamButton = new System.Windows.Forms.Button();
            this.resultButton = new System.Windows.Forms.Button();
            this.raceButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.circuitButton = new System.Windows.Forms.Button();
            this.switchPanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // driverButton
            // 
            this.driverButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.driverButton.BackColor = System.Drawing.Color.Red;
            this.driverButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.driverButton.ForeColor = System.Drawing.Color.White;
            this.driverButton.Location = new System.Drawing.Point(125, 444);
            this.driverButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.driverButton.Name = "driverButton";
            this.driverButton.Size = new System.Drawing.Size(195, 45);
            this.driverButton.TabIndex = 1;
            this.driverButton.Text = "DRIVERS";
            this.driverButton.UseVisualStyleBackColor = false;
            this.driverButton.Click += new System.EventHandler(this.DriverButton_Click);
            // 
            // teamButton
            // 
            this.teamButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.teamButton.BackColor = System.Drawing.Color.Red;
            this.teamButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.teamButton.ForeColor = System.Drawing.Color.White;
            this.teamButton.Location = new System.Drawing.Point(125, 501);
            this.teamButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.teamButton.Name = "teamButton";
            this.teamButton.Size = new System.Drawing.Size(195, 45);
            this.teamButton.TabIndex = 4;
            this.teamButton.Text = "TEAMS";
            this.teamButton.UseVisualStyleBackColor = false;
            this.teamButton.Click += new System.EventHandler(this.TeamButton_Click);
            // 
            // resultButton
            // 
            this.resultButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.resultButton.BackColor = System.Drawing.Color.Red;
            this.resultButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.resultButton.ForeColor = System.Drawing.Color.White;
            this.resultButton.Location = new System.Drawing.Point(395, 444);
            this.resultButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.resultButton.Name = "resultButton";
            this.resultButton.Size = new System.Drawing.Size(195, 45);
            this.resultButton.TabIndex = 2;
            this.resultButton.Text = "RESULTS";
            this.resultButton.UseVisualStyleBackColor = false;
            this.resultButton.Click += new System.EventHandler(this.ResultButton_Click);
            // 
            // raceButton
            // 
            this.raceButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.raceButton.BackColor = System.Drawing.Color.Red;
            this.raceButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.raceButton.ForeColor = System.Drawing.Color.White;
            this.raceButton.Location = new System.Drawing.Point(395, 501);
            this.raceButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.raceButton.Name = "raceButton";
            this.raceButton.Size = new System.Drawing.Size(195, 45);
            this.raceButton.TabIndex = 5;
            this.raceButton.Text = "RACES";
            this.raceButton.UseVisualStyleBackColor = false;
            this.raceButton.Click += new System.EventHandler(this.RaceButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.exitButton.BackColor = System.Drawing.Color.Red;
            this.exitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitButton.ForeColor = System.Drawing.Color.White;
            this.exitButton.Location = new System.Drawing.Point(665, 501);
            this.exitButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(195, 45);
            this.exitButton.TabIndex = 6;
            this.exitButton.Text = "EXIT";
            this.exitButton.UseVisualStyleBackColor = false;
            this.exitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // circuitButton
            // 
            this.circuitButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.circuitButton.BackColor = System.Drawing.Color.Red;
            this.circuitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.circuitButton.ForeColor = System.Drawing.Color.White;
            this.circuitButton.Location = new System.Drawing.Point(665, 444);
            this.circuitButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.circuitButton.Name = "circuitButton";
            this.circuitButton.Size = new System.Drawing.Size(195, 45);
            this.circuitButton.TabIndex = 3;
            this.circuitButton.Text = "CIRCUITS";
            this.circuitButton.UseVisualStyleBackColor = false;
            this.circuitButton.Click += new System.EventHandler(this.CircuitButton_Click);
            // 
            // switchPanel
            // 
            this.switchPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.switchPanel.BackColor = System.Drawing.Color.Transparent;
            this.switchPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.switchPanel.Location = new System.Drawing.Point(12, 12);
            this.switchPanel.Name = "switchPanel";
            this.switchPanel.Size = new System.Drawing.Size(960, 376);
            this.switchPanel.TabIndex = 0;
            this.switchPanel.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.SwitchPanel_ControlAdded);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::FormulaOne.Properties.Resources.extreem;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.switchPanel);
            this.Controls.Add(this.circuitButton);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.raceButton);
            this.Controls.Add(this.resultButton);
            this.Controls.Add(this.teamButton);
            this.Controls.Add(this.driverButton);
            this.Font = new System.Drawing.Font("Yu Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1000, 600);
            this.Name = "MainForm";
            this.Text = "Formula 1";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button driverButton;
        private System.Windows.Forms.Button teamButton;
        private System.Windows.Forms.Button resultButton;
        private System.Windows.Forms.Button raceButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button circuitButton;
        private System.Windows.Forms.Panel switchPanel;
    }
}

