﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace FormulaOne
{
    public partial class AddCircuitForm : Form
    {
        public AddCircuitForm()
        {
            InitializeComponent();
        }

        private void SelectLayout_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Image Files|*.bmp;*.png;*.jpg;*.jpeg"
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    layoutPictureBox.Image = Image.FromFile(openFileDialog.FileName);
                    selectLabel.Dispose();
                }
                catch (Exception) { }
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                var memoryStream = new MemoryStream();
                layoutPictureBox.Image.Save(memoryStream, ImageFormat.Jpeg);
                var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "INSERT INTO CIRCUIT VALUES(NULL, @NAME, @LAYOUT, @LOCATION, @COUNTRY)";
                command.Parameters.AddWithValue("@NAME", nameTextBox.Text);
                command.Parameters.AddWithValue("@LAYOUT", memoryStream.ToArray());
                command.Parameters.AddWithValue("@LOCATION", locationTextBox.Text);
                command.Parameters.AddWithValue("@COUNTRY", countryComboBox.SelectedValue);
                command.ExecuteNonQuery();
                connection.Close();
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception)
            {
                errorLabel.Show();
            }
        }

        private void AddCircuitForm_Load(object sender, EventArgs e)
        {
            var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT ID, NAME FROM COUNTRY JOIN `GRAND PRIX` ON ID = COUNTRY_ID";
            var reader = command.ExecuteReader();
            List<object> countries = new List<object>();
            while (reader.Read())
            {
                countries.Add(new
                {
                    ID = reader.GetInt32(0),
                    Name = reader.GetString(1)
                });
            }
            reader.Close();
            countryComboBox.DataSource = countries;
            countryComboBox.ValueMember = "ID";
            countryComboBox.DisplayMember = "Name";
        }

        private void AddCountryButton_Click(object sender, EventArgs e)
        {
            if (new AddCountryForm().ShowDialog() == DialogResult.OK)
            {
                AddCircuitForm_Load(null, null);
            }
        }
    }
}
