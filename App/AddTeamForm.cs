﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace FormulaOne
{
    public partial class AddTeamForm : Form
    {
        public AddTeamForm()
        {
            InitializeComponent();
        }

        private void SelectEmblemButton_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Image Files|*.bmp;*.png;*.jpg;*.jpeg"
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    emblemPictureBox.Image = Image.FromFile(openFileDialog.FileName);
                }
                catch (Exception) { }
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                var memoryStream = new MemoryStream();
                emblemPictureBox.Image.Save(memoryStream, ImageFormat.Jpeg);
                var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "INSERT INTO TEAM VALUES(NULL, @ALIAS, @FULL_NAME, @BASE, @EMBLEM, @COUNTRY)";
                command.Parameters.AddWithValue("@ALIAS", aliasTextBox.Text);
                command.Parameters.AddWithValue("@FULL_NAME", fullNameTextBox.Text);
                command.Parameters.AddWithValue("@BASE", baseTextBox.Text);
                command.Parameters.AddWithValue("@EMBLEM", memoryStream.ToArray());
                command.Parameters.AddWithValue("@COUNTRY", countryComboBox.SelectedValue);
                command.ExecuteNonQuery();
                connection.Close();
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception)
            {
                errorLabel.Show();
            }
        }

        private void AddTeamForm_Load(object sender, EventArgs e)
        {
            var connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["F1"].ConnectionString);
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = "SELECT ID, NAME FROM COUNTRY";
            var reader = command.ExecuteReader();
            List<object> countries = new List<object>();
            while (reader.Read())
            {
                countries.Add(new
                {
                    ID = reader.GetInt32(0),
                    Name = reader.GetString(1)
                });
            }
            reader.Close();
            countryComboBox.DataSource = countries;
            countryComboBox.ValueMember = "ID";
            countryComboBox.DisplayMember = "Name";
        }

        private void AddCountryButton_Click(object sender, EventArgs e)
        {
            if (new AddCountryForm().ShowDialog() == DialogResult.OK)
            {
                AddTeamForm_Load(null, null);
            }
        }
    }
}
